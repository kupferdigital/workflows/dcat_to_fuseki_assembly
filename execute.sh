#!/bin/bash

# Get RPT
wget https://github.com/SmartDataAnalytics/RdfProcessingToolkit/releases/download/v1.9.8-rc1/rpt-1.9.8-rc1.jar -O ./rpt.jar

# Get the catalog file
wget https://ckan.kupferdigital.org/catalog.ttl

# Execute construct query
java -jar ./rpt.jar integrate catalog.ttl construct.rq -o assembly.ttl

# Execute insert query
java -jar ./rpt.jar integrate assembly.ttl insert.rq spo.rq -o assembly2.ttl

# Send assembly file to Fuseki
# TODO send assembly correctly - atm unsure how to provide it as assembly not data
curl -X POST  -u "admin:$CKAN_ADMIN_API_KEY" -H "Content-Type: text/turtle" --data-binary "@assembly.ttl" https://ckan.kupferdigital.org/datasets